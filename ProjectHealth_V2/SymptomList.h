//
//  SymptomList.h
//  ProjectHealth_V2
//
//  Created by sree on 12/05/15.
//  Copyright (c) 2015 MAD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SymptomList : UIViewController<UITableViewDataSource,UITableViewDelegate>


@property(strong, nonatomic) IBOutlet UIBarButtonItem *leftSymptomBar;

@property(strong, nonatomic) IBOutlet UIBarButtonItem *rightSymptomBar;



@property(strong,nonatomic)  IBOutlet UIToolbar *listSelection;
@property(strong,nonatomic) UISegmentedControl *segmentForListSelection;


@property(nonatomic,strong) UIView *viewImageSymptoms;
@property(nonatomic,strong) UIView *viewListSymptoms;

@property(nonatomic,strong) UITableView *tableSymptoms;

@property(nonatomic, strong) UISearchController *searching;

@property(strong,nonatomic) NSMutableArray *searchArray;
@end
