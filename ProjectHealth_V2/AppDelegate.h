//
//  AppDelegate.h
//  ProjectHealth_V2
//
//  Created by MAD on 12/05/15.
//  Copyright (c) 2015 MAD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"



@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    Reachability *internetReachable;
    Reachability *hostReachable;

    
    
}


@property BOOL dataNeedsUpDate;
@property BOOL appNeedsUpDate;

@property BOOL internetActive;
@property BOOL hostActive;


@property (strong, nonatomic) UIWindow *window;


@end

