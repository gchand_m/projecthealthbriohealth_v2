//
//  ProceduresVC.m
//  ProjectHealth_V2
//
//  Created by MAD on 14/05/15.
//  Copyright (c) 2015 MAD. All rights reserved.
//

#import "ProceduresVC.h"
#import "ProcedureClass.h"


@interface ProceduresVC (){
    
    CGSize screenSize;
    
    NSMutableArray *fullProceduresList;
    NSMutableArray *searchProceduresList;
    NSMutableArray *searchArray;
    
    NSMutableArray *indexArray;
    NSInteger indexCount;
    
    NSMutableArray *indexedSections;
}

@property(nonatomic, strong) UISearchController *proceduresSearch;

@property(strong, nonatomic) UILocalizedIndexedCollation *collation;





@end

@implementation ProceduresVC

@synthesize collation,proceduresTable,proceduresSearch,toolProcedures;



-(void) viewWillAppear:(BOOL)animated{
    
    self.navigationItem.title=@"Procedures";
    
    searchArray =[[NSMutableArray alloc] init];
    
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"ProceduresData.plist"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
    
    NSLog(@"aa %ld ",[[dict objectForKey:@"Proce"] count]);
    fullProceduresList= [[NSMutableArray alloc] init];
    
    for (NSInteger i=0; i<[[dict objectForKey:@"Condi"] count]; i++) {
        
        ProcedureClass *temp2=[[ProcedureClass alloc] init];
        
        temp2.name=[[[dict objectForKey:@"Condi" ] objectAtIndex:i] objectForKey:@"name"];
        
        
        
        
        [fullProceduresList addObject:temp2];
    }
    
    
    collation= [UILocalizedIndexedCollation currentCollation];
    indexCount=[[collation sectionTitles] count];
    indexedSections=[[NSMutableArray alloc] initWithCapacity:indexCount];
    for (NSInteger tt=0; tt<indexCount; tt++) {
        
        [indexedSections addObject:[NSMutableArray array]];
    }
    
    
    for(int pt=0;pt<[fullProceduresList count];pt++){
        
        
        NSInteger sectionNumber=[collation sectionForObject:[fullProceduresList objectAtIndex:pt] collationStringSelector:@selector(name)];
        [indexedSections[sectionNumber] addObject:[fullProceduresList objectAtIndex:pt] ];
        
    }
    
    
    
    NSLog(@" %ld ",[indexedSections[7] count]);
    
    
}









- (void)viewDidLoad {
    [super viewDidLoad];
   
    screenSize=[UIScreen mainScreen].bounds.size;
    proceduresSearch =[[UISearchController alloc] initWithSearchResultsController:nil];
    [proceduresSearch.searchBar setFrame:CGRectMake(0, 0, screenSize.width, 50)];
    proceduresSearch.searchResultsUpdater=self;
    // medicationSearch.delegate=self;
    proceduresSearch.searchBar.delegate=self;
    proceduresSearch.dimsBackgroundDuringPresentation=NO;
    [toolProcedures addSubview:self.proceduresSearch.searchBar];
    
    //  medicationTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 120, screenSize.width, screenSize.height)];
    proceduresTable.delegate=self;
    proceduresTable.dataSource=self;





}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    CGPoint targetPoint = *targetContentOffset;
    CGPoint currentPoint = scrollView.contentOffset;
    
    if (targetPoint.y > currentPoint.y) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    else {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellID=@"myid";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    
    
    
    cell.textLabel.text=[[[indexedSections objectAtIndex:indexPath.section ]  objectAtIndex:indexPath.row] name];
    return cell;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (proceduresSearch.active) {
        return [indexedSections count];
        
    }
    
    return [indexedSections count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[indexedSections objectAtIndex:section] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    
    
    if([[indexedSections objectAtIndex:section] count]==0)
    {
        return nil;
    }
    
    
    return [[collation sectionTitles] objectAtIndex:section];
    
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    //  NSLog(@" 2 is 1");
    NSMutableArray *finalIndexList = [[NSMutableArray alloc ] init];
    
    
    for (NSInteger i=0; i<[indexedSections count]; i++) {
        
        if ([indexedSections[i] count]>0) {
            [finalIndexList addObject:[[[UILocalizedIndexedCollation currentCollation]   sectionIndexTitles] objectAtIndex:i]];
            
        }
        
    }

    
    
    // [finalIndexList addObjectsFromArray:[[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
    
    return finalIndexList;
    
}



- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    
    return index;
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    NSString *searchString = proceduresSearch.searchBar.text;
    
    if ([searchString length]==0 ) {
        collation= [UILocalizedIndexedCollation currentCollation];
        indexCount=[[collation sectionTitles] count];
        indexedSections=[[NSMutableArray alloc] initWithCapacity:indexCount];
        for (NSInteger tt=0; tt<indexCount; tt++) {
            
            [indexedSections addObject:[NSMutableArray array]];
        }
        
        
        for(int pt=0;pt<[fullProceduresList count];pt++){
            NSInteger sectionNumber=[collation sectionForObject:[fullProceduresList objectAtIndex:pt] collationStringSelector:@selector(name)];
            [indexedSections[sectionNumber] addObject:[fullProceduresList objectAtIndex:pt] ];
        }
        
        
        
    } else{
        [indexedSections removeAllObjects];
        [searchArray removeAllObjects];
        
        for (NSInteger inx=0; inx<[fullProceduresList count]; inx++) {
            
            if ([[[fullProceduresList objectAtIndex:inx] name ] localizedCaseInsensitiveContainsString:searchString]) {
                [searchArray addObject:[fullProceduresList objectAtIndex:inx]];
            }
            
            
        }
        
        for (NSInteger tt=0; tt<indexCount; tt++) {
            [indexedSections addObject:[NSMutableArray array]];
        }
        
        for(int pt=0;pt<[searchArray count];pt++){
            
            //   NSLog(@" %@ ",[[fullMedicationList objectAtIndex:pt] name ]);
            NSInteger sectionNumber=[collation sectionForObject:[searchArray objectAtIndex:pt] collationStringSelector:@selector(name)];
            //  NSLog(@" %d ",sectionNumber);
            
            [indexedSections[sectionNumber] addObject:[searchArray objectAtIndex:pt] ];
            
        }
        
        
        
        NSLog(@" new count %lu ",(unsigned long)[searchArray count]);
        
        
    }
    
    
    
    
    
    [self.proceduresTable reloadData];
}


//http://52.74.163.60/api/conditions/abdominal-swelling

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@" new data new data  ");
    
    
    NSLog(@"  name %@  ",[[[indexedSections objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] name] );
    
    
    NSURL *hostPath=[NSURL URLWithString:@"http://52.74.163.60/api/conditions/abdominal-swelling"];
    
    NSData *dataFromPath=[NSData dataWithContentsOfURL:hostPath];
    
    
    
    NSDictionary *jsonDic=[NSJSONSerialization JSONObjectWithData:dataFromPath options:0 error:nil ];
    
    
    NSLog(@" new obj  %@ ", jsonDic);
    
    
}

- (NSString *)name {
    NSString *title;
    // some code to fill title with an identifier for your object
    return title;
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
