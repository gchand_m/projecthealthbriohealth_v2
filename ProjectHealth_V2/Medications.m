//
//  Medications.m
//  ProjectHealth_V2
//
//  Created by MAD on 14/05/15.
//  Copyright (c) 2015 MAD. All rights reserved.
//

#import "Medications.h"
#import <RestKit/RestKit.h>
#import "MedicationClass.h"



@interface Medications (){
    
    CGSize screenSize;
    
    NSMutableArray *fullMedicationList;
    NSMutableArray *searchMedicationList;
    NSMutableArray *searchArray;
    
    NSMutableArray *indexArray;
    NSInteger indexCount;
  
    NSMutableArray *indexedSections;
}



//@property(strong,nonatomic) UITableView *medicationTable;

@property(nonatomic, strong) UISearchController *medicationSearch;

@property(strong, nonatomic) UILocalizedIndexedCollation *collation;

@property(strong, nonatomic) UISwipeGestureRecognizer *swip;

@end

@implementation Medications
@synthesize medicationSearch,medicationTable,collation,searchtool,swip;


-(void) viewWillAppear:(BOOL)animated{
    
       self.navigationItem.title=@"Medications";
    
    searchArray =[[NSMutableArray alloc] init];
    
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"MedicationsData.plist"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
   
    
    fullMedicationList= [[NSMutableArray alloc] init];
    
    for (NSInteger i=0; i<[[dict objectForKey:@"medic"] count]; i++) {
        
        MedicationClass *temp2=[[MedicationClass alloc] init];
        
        temp2.name=[[[dict objectForKey:@"medic" ] objectAtIndex:i] objectForKey:@"name"];
        temp2.med_id=[[[dict objectForKey:@"medic" ] objectAtIndex:i] objectForKey:@"medie"];
        temp2.isSalt=[[[dict objectForKey:@"medic" ] objectAtIndex:i] objectForKey:@"is_salt"];
        temp2.salt_id=[[[dict objectForKey:@"medic" ] objectAtIndex:i] objectForKey:@"salt_id"];
        

        
        
        [fullMedicationList addObject:temp2];
    }
    
 
    collation= [UILocalizedIndexedCollation currentCollation];
    indexCount=[[collation sectionTitles] count];
    indexedSections=[[NSMutableArray alloc] initWithCapacity:indexCount];
    for (NSInteger tt=0; tt<indexCount; tt++) {
        
        [indexedSections addObject:[NSMutableArray array]];
    }

    
       for(int pt=0;pt<[fullMedicationList count];pt++){

     //   NSLog(@" %@ ",[[fullMedicationList objectAtIndex:pt] name ]);
        NSInteger sectionNumber=[collation sectionForObject:[fullMedicationList objectAtIndex:pt] collationStringSelector:@selector(name)];
      //  NSLog(@" %d ",sectionNumber);

        [indexedSections[sectionNumber] addObject:[fullMedicationList objectAtIndex:pt] ];
    
    }

    
    
    NSLog(@" %ld ",[indexedSections[7] count]);
    
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    screenSize=[UIScreen mainScreen].bounds.size;
    medicationSearch =[[UISearchController alloc] initWithSearchResultsController:nil];
    [medicationSearch.searchBar setFrame:CGRectMake(0, 0, screenSize.width, 50)];
    medicationSearch.searchResultsUpdater=self;
       // medicationSearch.delegate=self;
    medicationSearch.searchBar.delegate=self;
    medicationSearch.dimsBackgroundDuringPresentation=NO;
    [searchtool addSubview:self.medicationSearch.searchBar];
    
  //  medicationTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 120, screenSize.width, screenSize.height)];
    medicationTable.delegate=self;
    medicationTable.dataSource=self;

    
    
    
    
    
    swip= [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipDetected)];
    swip.direction=UISwipeGestureRecognizerDirectionUp|UISwipeGestureRecognizerDirectionDown;
    [self.medicationTable addGestureRecognizer:swip];
    
  //  medicationTable.contentOffset= CGPointMake(0, medicationSearch.searchBar.bounds.size.height);
  //  [self.view addSubview:medicationTable];
    
    
    
//medicationSearch.searchBar

}


- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    CGPoint targetPoint = *targetContentOffset;
    CGPoint currentPoint = scrollView.contentOffset;
    
    if (targetPoint.y > currentPoint.y) {
        NSLog(@"up");
        //  self.navigationController.navigationBar.hidden=YES;
        //   self.navigationController.t
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    else {
        NSLog(@"down");
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//-(void) getRestData{
//    
//    
//    RKObjectMapping *medicationMapping = [RKObjectMapping mappingForClass:[MedicationClass class]];
//    [medicationMapping addAttributeMappingsFromDictionary:@{@"name":@"name",@"id" :@"med_id",@"salt_id":@"salt_id",@"is_salt":@"isSalt"}]; //,@"id" :@"med_id",@"salt_id":@"salt_id",@"is_salt":@"isSalt"
//    
//    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:medicationMapping
//                                                                                            method:RKRequestMethodAny
//                                                                                       pathPattern:nil
//                                                                                           keyPath:@"results"
//                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
//    
//    
//    
//    NSURL *url = [NSURL URLWithString:@"http://52.74.163.60/api/medications/?limit=300"];
//    
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
//    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
//        RKLogInfo(@"the result 'so called sucess': %@", [[result.array objectAtIndex:1] name] );
//        
//        
//        fullMedicationList =[[NSMutableArray alloc] initWithCapacity:[result count]];
//        
//        fullMedicationList.array=[result array];
//        
//        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Medications" ofType:@"plist"];
//        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
//        
//        
//        
//        
//        
//        for (NSInteger i=0; i<[fullMedicationList count]; i++) {
//            
//            NSDictionary *temp=[[NSDictionary alloc] initWithObjects:@[[[fullMedicationList objectAtIndex:i] name],
//                                                                     [[fullMedicationList objectAtIndex:i] med_id],
//                                                                     [[fullMedicationList objectAtIndex:1] salt_id],
//                                                                      [[fullMedicationList objectAtIndex:1] isSalt]] forKeys:@[@"name",@"med_id",@"salt_id",@"is_salt"]];
//            
//            
//            [[dict objectForKey:@"medic"] addObject:temp];
//            
//        //    [[dict objectForKey:@"medic"] objectAtIndex:i]
//            
//            
//            
//        }
//
//        
//        NSLog(@" %ld ",[[dict objectForKey:@"medic"] count] );
//        [dict writeToFile:filePath atomically:YES];
//        
//    }  failure:nil];
//    [operation start];
//    
//    
//}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    NSLog(@"Search  butt clicke");
   // [medicationTable setFrame:CGRectMake(0, 80, screenSize.width, screenSize.height)];
   // [self.view setNeedsLayout];

    
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
     NSLog(@"Search cancle butt clicke");
  //  [medicationTable setFrame:CGRectMake(0, 120, screenSize.width, screenSize.height)];
  //  [self.view setNeedsLayout];
    
}




//- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
//{
//     NSLog(@"some call");
//    [self updateSearchResultsForSearchController:self.medicationSearch];
//}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellID=@"myid";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    
   
    
    cell.textLabel.text=[[[indexedSections objectAtIndex:indexPath.section ]  objectAtIndex:indexPath.row] name];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (medicationSearch.active) {
        return [indexedSections count];
      
    }
    
    return [indexedSections count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[indexedSections objectAtIndex:section] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    
    
    if([[indexedSections objectAtIndex:section] count]==0)
    {
        return nil;
    }
    
    
    return [[collation sectionTitles] objectAtIndex:section];
    
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
   //  NSLog(@" 2 is 1");
    NSMutableArray *finalIndexList = [[NSMutableArray alloc ] init];
    
    
    for (NSInteger i=0; i<[indexedSections count]; i++) {
        
        if ([indexedSections[i] count]>0) {
            [finalIndexList addObject:[[[UILocalizedIndexedCollation currentCollation]   sectionIndexTitles] objectAtIndex:i]];
            
        }
        
    }
    
    
    
   // [finalIndexList addObjectsFromArray:[[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
    
    return finalIndexList;
    
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    
    return index;
}

//- (void)willPresentSearchController:(UISearchController *)searchController{
//    
//    NSLog(@" preasent  on search controller ");
//    
//}
//


- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{

    screenSize=[UIScreen mainScreen].bounds.size;
      NSString *searchString = medicationSearch.searchBar.text;
    
    if ([searchString length]==0 ) {
            collation= [UILocalizedIndexedCollation currentCollation];
            indexCount=[[collation sectionTitles] count];
            indexedSections=[[NSMutableArray alloc] initWithCapacity:indexCount];
            for (NSInteger tt=0; tt<indexCount; tt++) {
            
                [indexedSections addObject:[NSMutableArray array]];
            }
        
        
            for(int pt=0;pt<[fullMedicationList count];pt++){
                NSInteger sectionNumber=[collation sectionForObject:[fullMedicationList objectAtIndex:pt] collationStringSelector:@selector(name)];
                [indexedSections[sectionNumber] addObject:[fullMedicationList objectAtIndex:pt] ];
             }
        
        
    
    } else{
         [indexedSections removeAllObjects];
        [searchArray removeAllObjects];
        
        for (NSInteger inx=0; inx<[fullMedicationList count]; inx++) {
            
            if ([[[fullMedicationList objectAtIndex:inx] name ] localizedCaseInsensitiveContainsString:searchString]) {
                [searchArray addObject:[fullMedicationList objectAtIndex:inx]];
             }
            
            
        }

        for (NSInteger tt=0; tt<indexCount; tt++) {
            [indexedSections addObject:[NSMutableArray array]];
        }
        
        for(int pt=0;pt<[searchArray count];pt++){
            
            //   NSLog(@" %@ ",[[fullMedicationList objectAtIndex:pt] name ]);
            NSInteger sectionNumber=[collation sectionForObject:[searchArray objectAtIndex:pt] collationStringSelector:@selector(name)];
            //  NSLog(@" %d ",sectionNumber);
            
            [indexedSections[sectionNumber] addObject:[searchArray objectAtIndex:pt] ];
            
        }
        
        
        
        NSLog(@" new count %lu ",(unsigned long)[searchArray count]);
        
        
    }
   
    
    
    
    
    [self.medicationTable reloadData];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@" new data new data  ");
    
    NSLog(@"  salt_id %@  ",[[[indexedSections objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] salt_id] );
    NSLog(@"  name %@  ",[[[indexedSections objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] name] );
    NSLog(@"  is salt %@  ",[[[indexedSections objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isSalt] );
    NSLog(@" med id %@  ",[[[indexedSections objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] med_id] );
}







- (NSString *)name {
    NSString *title;
    // some code to fill title with an identifier for your object
    return title;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
