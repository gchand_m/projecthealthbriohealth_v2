//
//  Medications.h
//  ProjectHealth_V2
//
//  Created by MAD on 14/05/15.
//  Copyright (c) 2015 MAD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Medications : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchControllerDelegate,UISearchResultsUpdating,UISearchDisplayDelegate>



@property (weak, nonatomic) IBOutlet UIToolbar *searchtool;


@property (strong, nonatomic) IBOutlet UITableView *medicationTable;



@end
