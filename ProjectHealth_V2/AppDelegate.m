//
//  AppDelegate.m
//  ProjectHealth_V2
//
//  Created by MAD on 12/05/15.
//  Copyright (c) 2015 MAD. All rights reserved.
//

#import "AppDelegate.h"
#import "Reachability.h"

#import <RestKit/RestKit.h>
#import "MedicationClass.h"



#import "symptoms_class.h"
#import "Symptom_synonymClass.h"
#import "SymptomList.h"
#import "ConditionsClass.h"
#import "ProcedureClass.h"



@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize dataNeedsUpDate,appNeedsUpDate;

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *DataplistPath = [rootPath stringByAppendingPathComponent:@"DataState.plist"];
    
    NSDictionary *dataState=[[NSDictionary alloc] initWithObjects:@[@"NO",@"NO",@"NO",@"NO"] forKeys:@[@"SymptomsData",@"MedicationsData",@"ProceduresData",@"Conditions"]];
    [dataState writeToFile:DataplistPath atomically:YES];
     
     
    
    
    NSLog(@" %d  ", appNeedsUpDate);
    
    return YES;
}







- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    
    
    
    
  
    
    
    
  //
  //           reachability code
  //
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
      //  NSLog(@"There IS NO internet connection");
    } else {
      //  NSLog(@"There IS internet connection");
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    internetReachable = [Reachability reachabilityForInternetConnection];
    [internetReachable startNotifier];
    
    hostReachable = [Reachability reachabilityWithHostName:@"www.apple.com"] ; // update to new URL
    [hostReachable startNotifier];

    
    
    
    UIStoryboard *storylink=[UIStoryboard storyboardWithName:@"Main" bundle: nil];

    
    
   
    
    
    UITabBarController *viewStart=[storylink instantiateViewControllerWithIdentifier:@"1234"];
    UIViewController *temp_sympt=[storylink instantiateViewControllerWithIdentifier:@"2345"];
    UIViewController *temp_Cond=[storylink instantiateViewControllerWithIdentifier:@"2346"];
    UIViewController *temp_User=[storylink instantiateViewControllerWithIdentifier:@"UserVC3543"];
    UIViewController *temp_Medi=[storylink instantiateViewControllerWithIdentifier:@"Medication8844"];
    UIViewController *temp_Proce=[storylink instantiateViewControllerWithIdentifier:@"Procedures5623"];
    
    
    UINavigationController *navS=[[UINavigationController alloc] initWithRootViewController:temp_sympt];
    UINavigationController *navCon=[[UINavigationController alloc] initWithRootViewController:temp_Cond];
    UINavigationController *navMedi=[[UINavigationController alloc] initWithRootViewController:temp_Medi];
    UINavigationController *navProce=[[UINavigationController alloc] initWithRootViewController:temp_Proce];
    
    
    
    navS.tabBarItem.title=@"Symptoms";
    navS.tabBarItem.image=[UIImage imageNamed:@"Symptoms_Icon_gray.png"];
    navS.tabBarItem.selectedImage=[UIImage imageNamed:@"Symptoms_Icon_blue.png"];
    
    navCon.tabBarItem.title=@"Conditions";
    navCon.tabBarItem.image=[UIImage imageNamed:@"Conditions_Icon_gray.png"];
    navCon.tabBarItem.selectedImage=[UIImage imageNamed:@"Conditions_Icon_blue.png"];

    temp_User.tabBarItem.title=@"My Health";
    
    navMedi.tabBarItem.title=@"Medications";
    navMedi.tabBarItem.image=[UIImage imageNamed:@"Medications_Icon_gray.png"];
    navMedi.tabBarItem.selectedImage=[UIImage imageNamed:@"Medications_Icon_blue.png"];

    navProce.tabBarItem.title=@"Procedures";
    navProce.tabBarItem.image=[UIImage imageNamed:@"Procedures_Icon_gray.png"];
    navProce.tabBarItem.selectedImage=[UIImage imageNamed:@"Procedures_Icon_blue.png"];
    

    
    
    
    viewStart.viewControllers=@[navS,navCon,temp_User,navMedi,navProce];
    
    self.window.rootViewController=viewStart;

    NSLog(@"it is a good place for download? 22 ");

    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//        
//        [self getRestData];
//    });
//    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//        
//        [self configerRestKit];
//    });
//
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),^{
//        
//        [self getRestKitConditions];
//        
//    });
//    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),^{
//        
//        [self getRestKitProcedures];
//        
//    });

    
    return YES;
}














- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}





- (void) checkNetworkStatus:(NSNotification *)notice {
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)
    
    {
        case NotReachable:
        {
            NSLog(@"The internet is down.");
            self.internetActive = NO;
            
            break;
            
        }
        case ReachableViaWiFi:
        {
            NSLog(@"The internet is working via WIFI.");
            self.internetActive = YES;
            
            break;
            
        }
        case ReachableViaWWAN:
        {
            NSLog(@"The internet is working via WWAN.");
            self.internetActive = YES;
            
            break;
            
        }
    }
    
    NetworkStatus hostStatus = [hostReachable currentReachabilityStatus];
    switch (hostStatus)
    
    {
        case NotReachable:
        {
            NSLog(@"A gateway to the host server is down.");
            self.hostActive = NO;
            
            break;
            
        }
        case ReachableViaWiFi:
        {
            NSLog(@"A gateway to the host server is working via WIFI.");
            self.hostActive = YES;
            
            break;
            
        }
        case ReachableViaWWAN:
        {
            NSLog(@"A gateway to the host server is working via WWAN.");
            self.hostActive = YES;
            
            break;
            
        }
    }
    
}








-(void) getRestData{
    
    
    RKObjectMapping *medicationMapping = [RKObjectMapping mappingForClass:[MedicationClass class]];
    [medicationMapping addAttributeMappingsFromDictionary:@{@"name":@"name",@"id" :@"med_id",@"salt_id":@"salt_id",@"is_salt":@"isSalt"}]; //,@"id" :@"med_id",@"salt_id":@"salt_id",@"is_salt":@"isSalt"
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:medicationMapping
                                                                                            method:RKRequestMethodAny
                                                                                       pathPattern:nil
                                                                                           keyPath:@"results"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    
    ///http://52.74.163.60api/conditions/abdominal-swelling
    NSURL *url = [NSURL URLWithString:@"http://52.74.163.60/api/medications/?limit=3000"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
    RKLogInfo(@"the result 'so called success': %@", [[result.array objectAtIndex:1] name] );
        
        
    NSMutableArray *fullMedicationList =[[NSMutableArray alloc] initWithCapacity:[result count]];
        
    fullMedicationList.array=[result array];
        
        
        
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"MedicationsData.plist"];
    NSString *DataplistPath = [rootPath stringByAppendingPathComponent:@"DataState.plist"];
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] initWithObjects:@[[NSMutableArray arrayWithCapacity:0]] forKeys:@[@"medic"]];
        
        for (NSInteger i=0; i<[fullMedicationList count]; i++) {
           
          NSDictionary *temp=[[NSDictionary alloc] initWithObjects:@[[[fullMedicationList objectAtIndex:i] name],
                                                                       [[fullMedicationList objectAtIndex:i] med_id],
                                                                       [[fullMedicationList objectAtIndex:i] salt_id],
                                                                       [[fullMedicationList objectAtIndex:i] isSalt]] forKeys:@[@"name",@"medie",@"salt_id",@"is_salt"]];
          [[dict objectForKey:@"medic"] addObject:temp];
        
        }
        
       
         [[NSDictionary dictionaryWithContentsOfFile:DataplistPath] setValue:@"YES" forKey:@"MedicationsData"] ;
         [[NSDictionary dictionaryWithContentsOfFile:DataplistPath] writeToFile:DataplistPath atomically:YES];
         
        NSLog(@" the length %ld ",[[dict objectForKey:@"medic"] count] );
        [dict writeToFile:plistPath atomically:YES];
    }  failure:nil];
    [operation start];
    
    
}



-(void) getRestKitConditions{
    
    
    RKObjectMapping *ConditionMapping = [RKObjectMapping mappingForClass:[ConditionsClass class]];
    [ConditionMapping addAttributeMappingsFromDictionary:@{@"name":@"name" }];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:ConditionMapping
                                                                                            method:RKRequestMethodAny
                                                                                       pathPattern:nil
                                                                                           keyPath:@"results"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    
    
    NSURL *url = [NSURL URLWithString:@"http://52.74.163.60/api/conditions/?limit=2000"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        RKLogInfo(@"the result 'so called condition sucess': %@", [[result.array objectAtIndex:1] name] );
        
        
        NSMutableArray *fullConditionsArray =[[NSMutableArray alloc] initWithCapacity:[result count]];
        
        fullConditionsArray.array=[result array];
        
        NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *plistPath = [rootPath stringByAppendingPathComponent:@"ConditionsData.plist"];
        NSString *DataplistPath = [rootPath stringByAppendingPathComponent:@"DataState.plist"];
        
        
        NSMutableDictionary *dict_conditions=[[NSMutableDictionary alloc] initWithObjects:@[[NSMutableArray arrayWithCapacity:0]] forKeys:@[@"Condi"]];
        
        
        for (NSInteger i=0; i<[fullConditionsArray count]; i++)
        {
            NSDictionary *temp=[[NSDictionary alloc] initWithObjects:@[[[fullConditionsArray objectAtIndex:i] name]]
                                                             forKeys:@[@"name"]];
            
            
            
            
            
            
            [[dict_conditions objectForKey:@"Condi"] addObject:temp];
        }
        
        
        NSLog(@" tt %ld",[[dict_conditions objectForKey:@"Condi"] count] );
        
        [dict_conditions writeToFile:plistPath atomically:YES];
        [[NSDictionary dictionaryWithContentsOfFile:DataplistPath] setValue:@"YES" forKey:@"ConditionsData"] ;
        [[NSDictionary dictionaryWithContentsOfFile:DataplistPath] writeToFile:DataplistPath atomically:YES];
        
    }  failure:nil];
    [operation start];
    
    
    
    
    
    
}




-(void) configerRestKit{
    
     RKObjectMapping *mainmap=[RKObjectMapping mappingForClass:[symptoms_class class]];
    
    RKObjectMapping *symptomMapping = [RKObjectMapping mappingForClass:[symptName class]];
    [symptomMapping addAttributeMappingsFromDictionary:@{@"name":@"tnamet",@"msp_enabled":@"msp_status",@"life_threatening":@"life_risk" }];
    
   [ mainmap addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"main_symptom" toKeyPath:@"sympt_x" withMapping:symptomMapping]];
    
    RKObjectMapping *symptomSynonymMapping = [RKObjectMapping mappingForClass:[Symptom_synonymClass class]];
    
       [symptomSynonymMapping addAttributeMappingsFromDictionary:@{@"name":@"name" }];
    
    
    
    RKRelationshipMapping *rel=[RKRelationshipMapping relationshipMappingFromKeyPath:@"synonyms" toKeyPath:@"synonym_array" withMapping:symptomSynonymMapping];
    [mainmap addPropertyMapping:rel];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mainmap
                                                                                            method:RKRequestMethodAny
                                                                                       pathPattern:nil
                                                                                           keyPath:@"results"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    
    
    NSURL *url = [NSURL URLWithString:@"http://52.74.163.60/api/symptoms_list/?limit=1000"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        RKLogInfo(@"the result 'so called sucess':" );
        
        
       NSMutableArray* fullSymptomAray =[[NSMutableArray alloc] initWithCapacity:[result count]];
        
        fullSymptomAray.array=[result array];
        
        NSMutableDictionary *symptomListdata=[[NSMutableDictionary alloc] initWithObjects:@[[NSMutableArray arrayWithCapacity:0]] forKeys:@[@"Sympt"]];
        
                 for (NSInteger i=0,k=0; i<[fullSymptomAray count ]; i++) {
                       for (NSInteger j=0; j<[[[fullSymptomAray objectAtIndex:i] synonym_array] count]; j++,k++) {
                
                           NSMutableDictionary *temp=[[NSMutableDictionary alloc] initWithObjects:@[
                                                                                                    [[[fullSymptomAray objectAtIndex:i] sympt_x] tnamet],
                                                                                                    [[[fullSymptomAray objectAtIndex:i] sympt_x] msp_status],
                                                                                                    [[[fullSymptomAray objectAtIndex:i] sympt_x] life_risk]]
                                                                                          forKeys:@[@"name",@"mspstatus",@"liferisk"]];
                           
                           
                
                [temp setObject:[[[[fullSymptomAray objectAtIndex:i] synonym_array] objectAtIndex:j] name] forKey: @"synonym_name"];

               [[symptomListdata objectForKey:@"Sympt"] addObject:temp] ;
             }
            
            
            
        }
        NSLog(@" data count at write %ld  ",[[symptomListdata objectForKey:@"Sympt"] count]);
        NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *symptomDataPath = [rootPath stringByAppendingPathComponent:@"SymptomsData.plist"];
        NSString *DataplistPath = [rootPath stringByAppendingPathComponent:@"DataState.plist"];
        

        [symptomListdata writeToFile:symptomDataPath atomically:YES];
        [[NSDictionary dictionaryWithContentsOfFile:DataplistPath] setValue:@"YES" forKey:@"SymptomsData"] ;
        [[NSDictionary dictionaryWithContentsOfFile:DataplistPath] writeToFile:DataplistPath atomically:YES];
    }  failure:nil];
    [operation start];
    
    
    
    
    
    
}



-(void) getRestKitProcedures{
    
    
    RKObjectMapping *ProcedureMapping = [RKObjectMapping mappingForClass:[ProcedureClass class]];
    [ProcedureMapping addAttributeMappingsFromDictionary:@{@"name":@"name" }];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:ProcedureMapping
                                                                                            method:RKRequestMethodAny
                                                                                       pathPattern:nil
                                                                                           keyPath:@"results"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    
    
    NSURL *url = [NSURL URLWithString:@"http://52.74.163.60/api/procedures/?limit=2000"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        RKLogInfo(@"the result 'so called condition sucess': %@", [[result.array objectAtIndex:1] name] );
        
        
        NSMutableArray *fullProceduresArray =[[NSMutableArray alloc] initWithCapacity:[result count]];
        
        fullProceduresArray.array=[result array];
        
        NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *plistPath = [rootPath stringByAppendingPathComponent:@"ProceduresData.plist"];
        NSString *DataplistPath = [rootPath stringByAppendingPathComponent:@"DataState.plist"];
        
        
        NSMutableDictionary *dict_procedures=[[NSMutableDictionary alloc] initWithObjects:@[[NSMutableArray arrayWithCapacity:0]] forKeys:@[@"Condi"]];
        
        
        for (NSInteger i=0; i<[fullProceduresArray count]; i++)
        {
            NSDictionary *temp=[[NSDictionary alloc] initWithObjects:@[[[fullProceduresArray objectAtIndex:i] name]]
                                                             forKeys:@[@"name"]];
            
            
            
            
            
            
            [[dict_procedures objectForKey:@"Condi"] addObject:temp];
        }
        
        
        NSLog(@" tt %ld",[[dict_procedures objectForKey:@"Condi"] count] );
        
        [dict_procedures writeToFile:plistPath atomically:YES];
        [[NSDictionary dictionaryWithContentsOfFile:DataplistPath] setValue:@"YES" forKey:@"ProceduresData"] ;
        [[NSDictionary dictionaryWithContentsOfFile:DataplistPath] writeToFile:DataplistPath atomically:YES];
        
    }  failure:nil];
    [operation start];
    
    
    
    
    
    
}







@end
