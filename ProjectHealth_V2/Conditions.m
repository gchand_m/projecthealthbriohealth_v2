//
//  Conditions.m
//  ProjectHealth_V2
//
//  Created by MAD on 14/05/15.
//  Copyright (c) 2015 MAD. All rights reserved.
//


#import "Conditions.h"
#import "ConditionsClass.h"
@interface Conditions (){
    
    CGSize screenSize;
    
    NSMutableArray *fullConditionsList;
    NSMutableArray *searchMedicationList;
    NSMutableArray *searchArray;
    
    NSMutableArray *indexArray;
    NSInteger indexCount;
    
    NSMutableArray *indexedSections;
}



//@property(strong,nonatomic) UITableView *medicationTable;

@property(nonatomic, strong) UISearchController *conditionsSearch;

@property(strong, nonatomic) UILocalizedIndexedCollation *collation;




@end

@implementation Conditions
@synthesize collation,conditionsSearch,conditionTable,searchtool;

-(void) viewWillAppear:(BOOL)animated{
    
    self.navigationItem.title=@"Conditions";
    
    searchArray =[[NSMutableArray alloc] init];
    
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"ConditionsData.plist"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
    
    NSLog(@"aa %ld ",[[dict objectForKey:@"Condi"] count]);
    fullConditionsList= [[NSMutableArray alloc] init];
    
    for (NSInteger i=0; i<[[dict objectForKey:@"Condi"] count]; i++) {
        
        ConditionsClass *temp2=[[ConditionsClass alloc] init];
        
        temp2.name=[[[dict objectForKey:@"Condi" ] objectAtIndex:i] objectForKey:@"name"];
        
        
        
        
        [fullConditionsList addObject:temp2];
    }
    
    
    collation= [UILocalizedIndexedCollation currentCollation];
    indexCount=[[collation sectionTitles] count];
    indexedSections=[[NSMutableArray alloc] initWithCapacity:indexCount];
    for (NSInteger tt=0; tt<indexCount; tt++) {
        
        [indexedSections addObject:[NSMutableArray array]];
    }
    
    
    for(int pt=0;pt<[fullConditionsList count];pt++){
        
      
        NSInteger sectionNumber=[collation sectionForObject:[fullConditionsList objectAtIndex:pt] collationStringSelector:@selector(name)];
              [indexedSections[sectionNumber] addObject:[fullConditionsList objectAtIndex:pt] ];
        
    }
    
    
    
    NSLog(@" %ld ",[indexedSections[7] count]);
    
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    screenSize=[UIScreen mainScreen].bounds.size;
    conditionsSearch =[[UISearchController alloc] initWithSearchResultsController:nil];
    [conditionsSearch.searchBar setFrame:CGRectMake(20, 0, screenSize.width-80, 50)];
    //conditionsSearch.searchBar.backgroundColor=[UIColor redColor];
//    
//    UIBarButtonItem *myButton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"copy.jpg"] style:UIBarButtonItemStylePlain target:self action:@selector(toolbarButtonPressed)];
//    
//    
//    myButton setf
    
    UIButton *mybutt=[[UIButton alloc] initWithFrame:CGRectMake(screenSize.width-50, 10, 30, 30)];
    [mybutt setImage:[UIImage imageNamed:@"copy.jpg"]  forState:UIControlStateNormal];
    
    [searchtool addSubview:mybutt];
    
    
    conditionsSearch.searchResultsUpdater=self;
    // medicationSearch.delegate=self;
    conditionsSearch.searchBar.delegate=self;
    conditionsSearch.dimsBackgroundDuringPresentation=NO;
    [searchtool addSubview:self.conditionsSearch.searchBar];
    
    //  medicationTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 120, screenSize.width, screenSize.height)];
    conditionTable.delegate=self;
    conditionTable.dataSource=self;
    
}


- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    CGPoint targetPoint = *targetContentOffset;
    CGPoint currentPoint = scrollView.contentOffset;
    
    if (targetPoint.y > currentPoint.y) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    else {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellID=@"myid";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    
    
    
    cell.textLabel.text=[[[indexedSections objectAtIndex:indexPath.section ]  objectAtIndex:indexPath.row] name];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (conditionsSearch.active) {
        return [indexedSections count];
        
    }
    
    return [indexedSections count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[indexedSections objectAtIndex:section] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    
    
    if([[indexedSections objectAtIndex:section] count]==0)
    {
        return nil;
    }
    
    
    return [[collation sectionTitles] objectAtIndex:section];
    
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    //  NSLog(@" 2 is 1");
    NSMutableArray *finalIndexList = [[NSMutableArray alloc ] init];
    
    
    for (NSInteger i=0; i<[indexedSections count]; i++) {
        
        if ([indexedSections[i] count]>0) {
            [finalIndexList addObject:[[[UILocalizedIndexedCollation currentCollation]   sectionIndexTitles] objectAtIndex:i]];
            
        }
        
    }
    
    
    
    // [finalIndexList addObjectsFromArray:[[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
    
    return finalIndexList;
    
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    
    return index;
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
      NSString *searchString = conditionsSearch.searchBar.text;
    
    if ([searchString length]==0 ) {
        collation= [UILocalizedIndexedCollation currentCollation];
        indexCount=[[collation sectionTitles] count];
        indexedSections=[[NSMutableArray alloc] initWithCapacity:indexCount];
        for (NSInteger tt=0; tt<indexCount; tt++) {
            
            [indexedSections addObject:[NSMutableArray array]];
        }
        
        
        for(int pt=0;pt<[fullConditionsList count];pt++){
            NSInteger sectionNumber=[collation sectionForObject:[fullConditionsList objectAtIndex:pt] collationStringSelector:@selector(name)];
            [indexedSections[sectionNumber] addObject:[fullConditionsList objectAtIndex:pt] ];
        }
        
        
        
    } else{
        [indexedSections removeAllObjects];
        [searchArray removeAllObjects];
        
        for (NSInteger inx=0; inx<[fullConditionsList count]; inx++) {
            
            if ([[[fullConditionsList objectAtIndex:inx] name ] localizedCaseInsensitiveContainsString:searchString]) {
                [searchArray addObject:[fullConditionsList objectAtIndex:inx]];
            }
            
            
        }
        
        for (NSInteger tt=0; tt<indexCount; tt++) {
            [indexedSections addObject:[NSMutableArray array]];
        }
        
        for(int pt=0;pt<[searchArray count];pt++){
            
            //   NSLog(@" %@ ",[[fullMedicationList objectAtIndex:pt] name ]);
            NSInteger sectionNumber=[collation sectionForObject:[searchArray objectAtIndex:pt] collationStringSelector:@selector(name)];
            //  NSLog(@" %d ",sectionNumber);
            
            [indexedSections[sectionNumber] addObject:[searchArray objectAtIndex:pt] ];
            
        }
        
        
        
        NSLog(@" new count %lu ",(unsigned long)[searchArray count]);
        
        
    }
    
    
    
    
    
    [self.conditionTable reloadData];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   
    
    
    NSLog(@"  name %@  ",[[[indexedSections objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] name] );
    
}







- (NSString *)name {
    NSString *title;
    // some code to fill title with an identifier for your object
    return title;
}



-(void) toolbarButtonPressed{
    
    
     NSLog(@" do something  ");
    
}




















- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}











/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
