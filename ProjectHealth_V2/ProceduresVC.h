//
//  ProceduresVC.h
//  ProjectHealth_V2
//
//  Created by MAD on 14/05/15.
//  Copyright (c) 2015 MAD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProceduresVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchControllerDelegate,UISearchResultsUpdating,UISearchDisplayDelegate>


@property (weak, nonatomic) IBOutlet UIToolbar *toolProcedures;

@property (weak, nonatomic) IBOutlet UITableView *proceduresTable;

@end
