//
//  SymptomList.m
//  ProjectHealth_V2
//
//  Created by sree on 12/05/15.
//  Copyright (c) 2015 MAD. All rights reserved.
//

#import "SymptomList.h"

#import <RestKit/RestKit.h>
#import "symptoms_class.h"

@interface SymptomList (){
    UIImageView *im_symptoms;
    UIView *view2, *adder;
    UIButton *buttonGender,*buttonImageFace, *asps;
    int im_gender;
    int fr_back;
    
    NSMutableDictionary   *receivedList;
    NSMutableArray *fullSymptomAray;
    NSMutableArray *loadingArray;
    NSArray *alphabetArray;
    NSMutableArray *indexedSections;
    
    NSMutableDictionary *symptomsForTable;
    
    CGRect screenView;
    NSInteger indexCount;
    
}
@property (nonatomic) UILocalizedIndexedCollation *collation;

@property(nonatomic,strong) NSMutableArray  *allSection;


@end

@implementation SymptomList
@synthesize listSelection,segmentForListSelection,viewImageSymptoms,viewListSymptoms,tableSymptoms,allSection,searching,searchArray,collation;

-(void) viewWillAppear:(BOOL)animated{
  
    screenView=[UIScreen mainScreen].bounds;
    
    fr_back=1;
    im_gender=1;
    //
    //   code for segment control & attaching it to navigation bar
    //
    
    
    
    for (UIView *view in self.navigationController.navigationBar.subviews)
    {
        for (view2 in view.subviews)
        {
            if ([view2 isKindOfClass:[UIImageView class]])
            {
                [view2 removeFromSuperview];
            }
        }
    }

    
    self.navigationItem.title=@"Symptoms";
    
    
    [self loadImageView_symptoms];
    
    if (!segmentForListSelection) {
     segmentForListSelection=[[UISegmentedControl alloc] initWithItems:@[@"ImageView",@"ListView"]];
         }
    [segmentForListSelection setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-100, 0, 200, 30)];
    
    [segmentForListSelection addTarget:self action:@selector(selectionProcess) forControlEvents:UIControlEventValueChanged];
    [listSelection addSubview:segmentForListSelection];
    
    
    segmentForListSelection.selectedSegmentIndex=0;
    [self loadImageView_symptoms];
    
    
    
    //
    // code for   search contoller
    //
    
    
    searching=[[UISearchController alloc] initWithSearchResultsController:nil];
    [searching.searchBar setFrame:CGRectMake(0, 0, self.tableSymptoms.bounds.size.width, 30)];
    searching.searchResultsUpdater=self;
    searching.hidesNavigationBarDuringPresentation=true;
    self.tableSymptoms.tableHeaderView=searching.searchBar;
    
    searchArray=[[NSMutableArray alloc] init];

    
    
    
}








- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSLog(@"wiew did appiar");
   
    
    
    
}







-(void) selectionProcess {
    
    switch (segmentForListSelection.selectedSegmentIndex) {
        case 0:
            [self loadImageView_symptoms];
            self.view.userInteractionEnabled=YES;
            break;
        case 1:
           [self loadTableView_symptoms];
            self.view.userInteractionEnabled=NO;   // some kind of loading indicator should start hear
            [self configerTableData];
            break;
            
    }
    
    
}



-(void)loadImageView_symptoms{
    
    if (viewListSymptoms) {
        [viewListSymptoms removeFromSuperview];
    }
    // viewImageSymptoms =[[UIView alloc] init];
    
    
    im_symptoms=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-180)];
    im_symptoms.image=[UIImage imageNamed:@"male_front.jpg"];
    im_symptoms.contentMode=  UIViewContentModeScaleAspectFit;
    viewImageSymptoms =[[UIView alloc] initWithFrame:CGRectMake(0, 110, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-120)];
    
    
    
    [viewImageSymptoms addSubview:im_symptoms];
    
    
    
    // [viewImageSymptoms setBackgroundColor:[UIColor blackColor]];
    buttonGender=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonGender setFrame:CGRectMake(50,(screenView.size.height*.6), 80, 50)];
    [buttonGender setTitle:@"Gender" forState:UIControlStateNormal];
    buttonGender.tag=3434;
    [buttonGender setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [buttonGender addTarget:self action:@selector(genderButton:) forControlEvents:UIControlEventTouchUpInside];
    [im_symptoms addSubview:buttonGender];
    [self addTouchFeaturesToImage];
    
    
    
    
    asps=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    [asps setFrame:CGRectMake(screenView.size.width*.65,(screenView.size.height*.6), 80, 50)];
    [asps setTitle:@"front/back" forState:UIControlStateNormal];
    [asps setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [asps addTarget:self action:@selector(genderButton:) forControlEvents:UIControlEventTouchUpInside];
    asps.tag=3436;
    
    
    
    [im_symptoms addSubview:asps];
    
    [self.view addSubview:viewImageSymptoms];
    
    
}


-(void)genderButton:(id)sender
{
    
    if ([(UIButton *)sender tag]==3434)
         im_gender++;
    else
         fr_back++;
        
       
        if (im_gender%2){

                  if (fr_back%2)
                im_symptoms.image=[UIImage imageNamed:@"male_front.jpg"];
            else
                im_symptoms.image=[UIImage imageNamed:@"male_back.jpg"];

        }
        else{
            if (fr_back%2)
                im_symptoms.image=[UIImage imageNamed:@"female_front.jpg"];
            else
                im_symptoms.image=[UIImage imageNamed:@"female_back.jpg"];
        }
   
        
}
    
   
    
       




-(void)loadTableView_symptoms{
    if (viewImageSymptoms) {
        [viewImageSymptoms removeFromSuperview];
    }
    
    viewListSymptoms =[[UIView alloc] initWithFrame:CGRectMake(0, 110, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-165)];
    
    tableSymptoms=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-165)];
    
    tableSymptoms.dataSource=self;
    tableSymptoms.delegate=self;
    
    [self.viewListSymptoms addSubview:tableSymptoms];
    
    
    
    
    [self.view addSubview:viewListSymptoms];
    
    
    
}


-(void) addTouchFeaturesToImage{
    
    im_symptoms.userInteractionEnabled=YES;
    
    
    
    
    
    
}





-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
     
         return [indexedSections count];
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *cellid = @"QuoteCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    
    if (cell == nil) {
        
        
        cell =[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
    }
    
    
//    if (self.searching.active) {
//        cell.textLabel.text=[searchArray objectAtIndex:indexPath.row];
//    }
//    else
    {
       cell.textLabel.text=[[[indexedSections objectAtIndex:indexPath.section ]  objectAtIndex:indexPath.row] name];
    }
    
    
    return cell;
}

//- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
//    
//    CGPoint targetPoint = *targetContentOffset;
//    CGPoint currentPoint = scrollView.contentOffset;
//    
//    if (targetPoint.y > currentPoint.y) {
//        NSLog(@"up");
//        //  self.navigationController.navigationBar.hidden=YES;
//        //   self.navigationController.t
//        [self.navigationController setNavigationBarHidden:YES animated:YES];
//    }
//    else {
//        NSLog(@"down");
//        [self.navigationController setNavigationBarHidden:NO animated:YES];
//    }
//}
//


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    NSMutableArray *finalIndexList = [[NSMutableArray alloc ] init];
    
    
    for (NSInteger i=0; i<[indexedSections count]; i++) {
        
        if ([indexedSections[i] count]>0) {
            [finalIndexList addObject:[[[UILocalizedIndexedCollation currentCollation]   sectionIndexTitles] objectAtIndex:i]];
            
        }
        
    }
    
    
    
    // [finalIndexList addObjectsFromArray:[[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
    
    return finalIndexList;
    
}




-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[indexedSections objectAtIndex:section] count];
}


- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    
    return index;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    
    
    if([[indexedSections objectAtIndex:section] count]==0)
    {
        return nil;
    }
    
    
    return [[collation sectionTitles] objectAtIndex:section];
    
}



-(void) configerTableData{
    
    
    
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *symptomDataPath = [rootPath stringByAppendingPathComponent:@"SymptomsData.plist"];
    NSMutableDictionary *dictSympt = [[NSMutableDictionary alloc] initWithContentsOfFile:symptomDataPath];
    
    fullSymptomAray =[[NSMutableArray alloc] init];
    
    symptomsForTable=[[NSMutableDictionary alloc] init];
    
//    NSLog( @" %ld ",[[dictSympt objectForKey:@"Sympt"] count]);
   
    
    
    for (NSInteger i=0; i<[[dictSympt objectForKey:@"Sympt"] count]; i++)
    {
        Symptom_synonymClass *syn=[[Symptom_synonymClass alloc] init];
        syn.name=[[[dictSympt objectForKey:@"Sympt"] objectAtIndex:i] objectForKey:@"synonym_name"];
        //  NSLog(@" val%@ ",  syn.name );
      
        
        NSDictionary *tempo=[[NSDictionary alloc] initWithObjects:@[
                                                                    [[[dictSympt objectForKey:@"Sympt"] objectAtIndex:i] objectForKey:@"name"],
                                                                    [[[dictSympt objectForKey:@"Sympt"] objectAtIndex:i] objectForKey:@"mspstatus"],
                                                                    [[[dictSympt objectForKey:@"Sympt"] objectAtIndex:i] objectForKey:@"liferisk"]
                                                                    ]
                                                          forKeys:@[@"symptom",@"mspstatus",@"liferisk"]];
        
       
        
        [symptomsForTable setObject:tempo forKey:syn.name];
        [fullSymptomAray addObject:syn];
        
    }
    
    //indexCount=[[NSNumber alloc] initWithInteger:5];
    
    collation=[UILocalizedIndexedCollation currentCollation];
    
       indexCount=[[collation sectionTitles] count];
    indexedSections=[[NSMutableArray alloc] initWithCapacity:indexCount];
    
    for (NSInteger tt=0; tt<indexCount; tt++) {
        
        [indexedSections addObject:[NSMutableArray array] ];
    }
    
    
      for(int pt=0;pt<[fullSymptomAray count];pt++){
        NSInteger sectionNumber=[collation sectionForObject:[fullSymptomAray  objectAtIndex:pt]  collationStringSelector:@selector(name)];
        [indexedSections[sectionNumber] addObject:[fullSymptomAray objectAtIndex:pt]  ];
        
    }
    
        [self.tableSymptoms reloadData];
        self.view.userInteractionEnabled=YES;
}





- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    
    NSString *searchString = @"dd";// conditionsSearch.searchBar.text;
    
    if ([searchString length]==0 ) {
        collation= [UILocalizedIndexedCollation currentCollation];
        indexCount=[[collation sectionTitles] count];
        indexedSections=[[NSMutableArray alloc] initWithCapacity:indexCount];
        for (NSInteger tt=0; tt<indexCount; tt++) {
            
            [indexedSections addObject:[NSMutableArray array]];
        }
        
        
        for(int pt=0;pt<[fullSymptomAray count];pt++){
            NSInteger sectionNumber=[collation sectionForObject:[fullSymptomAray objectAtIndex:pt] collationStringSelector:@selector(name)];
            [indexedSections[sectionNumber] addObject:[fullSymptomAray objectAtIndex:pt] ];
        }
        
        
        
    } else{
        [indexedSections removeAllObjects];
        [searchArray removeAllObjects];
        
        for (NSInteger inx=0; inx<[fullSymptomAray count]; inx++) {
            
            if ([[[fullSymptomAray objectAtIndex:inx] name ] localizedCaseInsensitiveContainsString:searchString]) {
                [searchArray addObject:[fullSymptomAray objectAtIndex:inx]];
            }
            
            
        }
        
        for (NSInteger tt=0; tt<indexCount; tt++) {
            [indexedSections addObject:[NSMutableArray array]];
        }
        
        for(int pt=0;pt<[searchArray count];pt++){
            
            //   NSLog(@" %@ ",[[fullMedicationList objectAtIndex:pt] name ]);
            NSInteger sectionNumber=[collation sectionForObject:[searchArray objectAtIndex:pt] collationStringSelector:@selector(name)];
            //  NSLog(@" %d ",sectionNumber);
            
            [indexedSections[sectionNumber] addObject:[searchArray objectAtIndex:pt] ];
            
        }
        
        
        
        NSLog(@" new count %lu ",(unsigned long)[searchArray count]);
        
        
    }
    
    
    
    
    
    [self.tableSymptoms reloadData];
}


- (NSString *)name {
    NSString *title;
    // some code to fill title with an identifier for your object
    return title;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    
    
    
}


@end
