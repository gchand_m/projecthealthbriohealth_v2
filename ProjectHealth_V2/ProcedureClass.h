//
//  ProcedureClass.h
//  ProjectHealth_V2
//
//  Created by MAD on 21/05/15.
//  Copyright (c) 2015 MAD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProcedureClass : NSObject

@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *slug;

@end
