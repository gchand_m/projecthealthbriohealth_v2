//
//  main.m
//  ProjectHealth_V2
//
//  Created by MAD on 12/05/15.
//  Copyright (c) 2015 MAD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
