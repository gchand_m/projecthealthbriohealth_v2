//
//  MedicationClass.h
//  ProjectHealth_V2
//
//  Created by MAD on 14/05/15.
//  Copyright (c) 2015 MAD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MedicationClass : NSObject

@property(strong,nonatomic)  NSString *med_id;
@property(strong,nonatomic) NSString *name;
@property(strong,nonatomic) NSString *salt_id;
@property(strong,nonatomic) NSString *isSalt;

@end
