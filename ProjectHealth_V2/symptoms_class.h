//
//  symptoms_class.h
//  ProjectHealth_V2
//
//  Created by MAD on 14/05/15.
//  Copyright (c) 2015 MAD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Symptom_synonymClass.h"
#import "symptName.h"



@interface symptoms_class : NSObject

@property(nonatomic) symptName *sympt_x;
@property(nonatomic, strong) NSArray *synonym_array;

@end
