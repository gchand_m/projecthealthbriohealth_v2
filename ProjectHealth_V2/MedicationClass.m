//
//  MedicationClass.m
//  ProjectHealth_V2
//
//  Created by MAD on 14/05/15.
//  Copyright (c) 2015 MAD. All rights reserved.
//

#import "MedicationClass.h"

@implementation MedicationClass
@synthesize salt_id,name,isSalt,med_id;


-(id) initWithName:(NSString *) medid:(NSString *)name_med: (NSString *)saltid: (NSString *)is_salt_bool {
   
    
    self = [super init];
    if(self){
        
        self.name=name_med;
        self.med_id=medid;
        self.isSalt=is_salt_bool;
        self.salt_id=saltid;
        
    }
    
    return self;
}

@end
