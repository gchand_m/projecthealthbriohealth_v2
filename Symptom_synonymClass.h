//
//  Symptom_synonymClass.h
//  ProjectHealth_V2
//
//  Created by MAD on 20/05/15.
//  Copyright (c) 2015 MAD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Symptom_synonymClass : NSObject

@property(nonatomic, strong) NSString *name;

@end
